import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DCSModel extends ChangeNotifier {
  
  String _spat="";

  String get spat=>_spat;

  void setUser(String spat) {

    _spat =spat;
    // Notify listeners will only update listeners of state.
    notifyListeners();
  }



}